﻿using UnityEngine;
using UnityEngine.Audio;

public class DummySounds : MonoBehaviour
{

    // Use this for initialization
    public static bool breathGiven = false;
    public static bool cprComplete = false;

    [Tooltip("The tracking device used to determine absolute direction of Head")]
    public CommonTracker hmd_tracker;

    AudioManager mgr;
    int count = 0;

    void Start()
    {
        mgr = FindObjectOfType<AudioManager>();

    }

    // Update is called once per frame
    void Update()
    {
        //Var to hold the audio source for footstep

        var rotation = hmd_tracker.transform.rotation.eulerAngles;
        if (breathGiven && !cprComplete)
        {
            print(rotation.x + " " + rotation.y);
            if (rotation.y > 75 && rotation.z > 75)
            {
                mgr.Play("DummyBreathing");
                count++;
                breathGiven = false;
            }
        }
        else if (count>5 && !cprComplete)
            {
                cprComplete = true;
                mgr.Play("choking");
                breathGiven = false;
            }
    }
}
