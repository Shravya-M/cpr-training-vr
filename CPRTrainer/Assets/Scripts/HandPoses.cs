﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HandPoses: MonoBehaviour {
	GameObject lefthand;
	GameObject righthand;
	GameObject bothhand;
    public CommonButton rightTriggerButton;
	public CommonAxis leftjoystick;
	public CommonAxis rightjoystick;
    public CommonTracker rightController;
    public CommonTracker leftController;
    public static bool bothHandAState;


	// Use this for initialization
	void Start () {

		//reading the right and left hand models
		lefthand = GameObject.FindGameObjectWithTag("LeftHand");
		righthand = GameObject.FindGameObjectWithTag("RightHand");

		//reading the curled hand
		bothhand = GameObject.FindGameObjectWithTag("CurledHand");
		bothhand.SetActive(false);
        bothHandAState = bothhand.activeSelf;

    }
	
	// Update is called once per frame
	void Update () {
	//double d=Math.Sqrt( Math.Pow(rightjoystick.GetAxis().x-leftjoystick.GetAxis().x,2) + Math.Pow(rightjoystick.GetAxis().y-leftjoystick.GetAxis().y,2));
	    

        if (Math.Abs(rightController.GetVirtualPosition().x - leftController.GetVirtualPosition().x) <= 0.1 &&
           Math.Abs(rightController.GetVirtualPosition().y - leftController.GetVirtualPosition().y) <= 0.2 &&
           Math.Abs(rightController.GetVirtualPosition().z - leftController.GetVirtualPosition().z) <= 0.1) {
            bothhand.SetActive(true);
            bothHandAState = bothhand.activeSelf;
            lefthand.SetActive(false);
            righthand.SetActive(false);
            //Debug.Log(d, bothhand);
        } else {
            bothhand.SetActive(false);
            bothHandAState = bothhand.activeSelf; 
            lefthand.SetActive(true);
            righthand.SetActive(true);
        }
    		
    		

	}
}
