﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateCounterBar : MonoBehaviour 
{
    public Slider cprSlider;
    public Slider breathSlider;
    CPRCounter cprCounter;
    Text cprNum;

    Text breathNum;

    public int counterBarCPR = 0;
    public int counterBarBreath = 0;

    private void Start()
    {
        cprCounter = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CPRCounter>();
        cprNum = GameObject.FindGameObjectWithTag("CPR_Num").GetComponent<Text>();
        breathNum = GameObject.FindGameObjectWithTag("Breath_Num").GetComponent<Text>();

    }

    public void udpateCounters() {
        counterBarCPR = cprCounter.cprCount;
        counterBarBreath = cprCounter.breathCount;
    }

    public void resetCPRBar() {
        cprSlider.value = 0;
    }

    public void resetBreathbar() {
        breathSlider.value = 0;
    }

    private void Update()
    {
        cprSlider.value = counterBarCPR;
        cprNum.text = cprSlider.value.ToString();

        if (cprSlider.value == 1)
        {
            resetBreathbar();
        }
        
        breathSlider.value = counterBarBreath;
        breathNum.text = breathSlider.value.ToString();

        if (breathSlider.value == 2)
        {
            resetCPRBar();
        } 
    }
}
