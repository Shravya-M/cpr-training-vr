﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class ErikSounds : MonoBehaviour {

    // Use this for initialization

    public CustomSound[] sounds;
    public static bool soundPlaying = false;
    EricJaw jawMotion = new EricJaw();
    CustomSound s;
    void Awake()
    {
        foreach (CustomSound s1 in sounds)
        {
            s1.source = gameObject.AddComponent<AudioSource>();
            s1.source.clip = s1.clip;
        }
        jawMotion.stopJaw();
    }

    private void Update()
    {
        if (s.source.isPlaying)
        {
            soundPlaying = true;
            jawMotion.startJaw();
        }
        else
        {
            soundPlaying = false;
            jawMotion.stopJaw();
        }
    }

    public void say(string name)
    {
        if(s!= null)
        {
            
            if (!s.name.Equals(name))
            {
                print(s.name + "Stopped. Now playing  " + name);
                s.source.Stop();
            }

        }
        s = Array.Find(sounds, sound => sound.name == name);
        if (!s.source.isPlaying)
        {
            s.source.Play();
            soundPlaying = true;

        }
        else
        {
            soundPlaying = false;
        }
    }
}
