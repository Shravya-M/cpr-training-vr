﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBreathing : MonoBehaviour {

    // Enumerate the states of travel
    public enum BreathingState
    {
        HeadAway,
        HeadClose
    };

    // Inspector parameters
    [Tooltip("The tracking device used to determine absolute direction of Head")]
    public CommonTracker hmd_tracker;

    [Tooltip("A button required to be pressed to activate breathing")]
    public CommonButton button;
    
    // Private interaction variables
    private BreathingState state;

    GameObject dummyMouth;
    bool breathingFlag = true;
    Counter c = new Counter();


    // Called at the end of the program initialization
    void Start()
    {

        // Set initial travel state to no travel
        state = BreathingState.HeadAway;
        dummyMouth = GameObject.FindGameObjectWithTag("DummyMouth");
    }

    // FixedUpdate is not called every graphical frame but rather every physics frame
    void FixedUpdate()
    {
        //Var to hold the audio source for footstep
         var audioSource = GetComponent<AudioSource>();

        //Get the distance between the hmd and the mouth of the dummy
        var hmd = hmd_tracker.transform.position;
        var dm = dummyMouth.transform.position;
        var distance = Vector3.Distance(dm,hmd);
       
        var rotation = hmd_tracker.transform.eulerAngles.x;
        float closeDistance = 0.2f;
        //print("distance = " + distance+"   Rotation = "+rotation);
        switch (state)
        {
            case BreathingState.HeadAway:
                {
                    
                    //if Head of the user is close to the dummy's mouth Change the state to HeadClose
                    if (distance <= closeDistance)
                    {
                            state = BreathingState.HeadClose;
                    }
                    else
                    {
                        // Nothing to do for not steering
                    }
                }
                break;

            case BreathingState.HeadClose:
                {
                    
                    // If the head moves away from the dummy
                    if (distance > closeDistance)
                    {
                        //  change the state to away
                        state = BreathingState.HeadAway;
                        DummySounds.breathGiven = false;
                    }
                    //If the head is close from the dummy and trigger is pressed
                    //else if (rotation > 75 && rotation <= 95 && button.GetPress())
                    
                    else if (rotation > 75 && rotation <= 95 && button.GetPress() && breathingFlag)
                    {
                        if (!audioSource.isPlaying)
                        {
                            audioSource.Play();
                            breathingFlag = false;
                            DummySounds.breathGiven = true;
                            c.updateBreathCounter(1);
                        }
                    }
                    else if(!button.GetPress() && !breathingFlag)
                    {
                        breathingFlag = true;
                    }
                }
                break;
        }
    }
}
