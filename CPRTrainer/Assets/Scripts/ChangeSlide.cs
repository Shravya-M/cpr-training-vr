﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class ChangeSlide : MonoBehaviour
{

    ErikSounds erik;
    //Inspector Parameters
    [Tooltip("A button required to be pressed to change the slides.")]
    public CommonButton trigger;

    [Tooltip("An axis to detect if user wants to go to previous slide or next slide.")]
    public CommonAxis joyStick;

    public Material[] material;
    Renderer rend;
    int currentSlide;
    int slideCount;
    bool flag = true;

    // Use this for initialization
    void Start()
    {
        erik = FindObjectOfType<ErikSounds>();
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        currentSlide = 0;
        slideCount = material.Length;
        rend.sharedMaterial = material[currentSlide];
        //if (!ErikSounds.soundPlaying)
        {
            erik.say("slide0");
        }
    }

    // Update is called once per frame
    void Update()
    {
        var currentAxis = joyStick.GetAxis();
        if (trigger.GetPress() && flag == true)
        {
            //if (!ErikSounds.soundPlaying)
            {
                if (currentAxis.x > 0)
                {
                    currentSlide = (currentSlide + 1) % slideCount;
                    flag = false;
                    rend.sharedMaterial = material[currentSlide];
                    erik.say("slide" + currentSlide);

                }
                else if (currentAxis.x < 0)
                {
                    currentSlide = (currentSlide - 1);
                    if (currentSlide < 0)
                    {
                        currentSlide = slideCount - 1;
                    }
                    flag = false;
                    rend.sharedMaterial = material[currentSlide];
                }
            }
        }
            else if (!trigger.GetPress() && flag == false)
            {
                flag = true;
            }

    }
}
