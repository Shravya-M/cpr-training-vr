﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Counter : MonoBehaviour 
{
    
    public static int cprCount = 0;
    public static int breathCount = 0;

    ErikSounds erik;

    private void Start() {
        erik = FindObjectOfType<ErikSounds>();
       
    }

	public void updateCPRCounter (int count)
	{
        //erik.say(1.ToString());
        if(count == 1)
        {
            if (cprCount < 30)
            {
                cprCount += count;
                Debug.Log("CPRCount = " + cprCount.ToString());
                erik = FindObjectOfType<ErikSounds>();
                erik.say(cprCount.ToString());

                if (cprCount == 1)
                {
                    breathCount = 0;
                }
            }
            else if(cprCount == 30)
            {
                Debug.Log("No More Compression");
                erik.say("noMoreCompression");
            }
        }
        else if( count == -1)
        {
            int rand = Random.Range(1,4);
            erik = FindObjectOfType<ErikSounds>();
            string say = "BadCompression" + rand;
            print("erik Says --> " + say);
            erik.say(say);

        }
        else if(count == 3)
        {
            print("erik Says --> incompleteCompressionCycle");
            erik.say("incompleteCompressionCycle");
        }

	}

    public void updateBreathCounter(int count)
    {
        if (breathCount < 2)
        {
            breathCount += count;
            Debug.Log("breathCount = " + breathCount.ToString());
            erik = FindObjectOfType<ErikSounds>();
            erik.say(breathCount.ToString());

            if (breathCount >= 1)
            {
                cprCount = 0;
            }
        }
    }
}
