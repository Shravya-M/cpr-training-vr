﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAnimation : MonoBehaviour
{

	private Animator _anim;
    private Animator _anim2;

	

	// Use this for initialization
	void Start()
	{
		_anim = GetComponentInChildren<Animator>();
        _anim2 = GetComponentInChildren<Animator>();
        //_anim.SetBool("IsOpen", true);
       // _anim.SetBool("IsGrabbing", true);
        _anim2.SetBool("IsOpen",true);
    }

	// Update is called once per frame
	void Update()
	{
		
		_anim.SetBool("IsGrabbing", true);
        //_anim.SetBool("IsOpen", true);
        //_anim.SetBool("IsGrabbing", false);
        //_anim.SetBool("IsOpen", false);

    }
}
