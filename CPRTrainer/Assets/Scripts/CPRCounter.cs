﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CPRCounter : MonoBehaviour 
{

    private UpdateCounterBar updateCounterBar;

	public int cprCount = 0;
    public int breathCount = 0;

    ErikSounds erik;


     private void Start()
    {
        erik = FindObjectOfType<ErikSounds>();
        updateCounterBar = GameObject.FindGameObjectWithTag("GameManager").GetComponent<UpdateCounterBar>();
        cprCount = 0;
        breathCount = 0;
    }

    public void incrementCPRCounter(int count)
	{
        if(cprCount < 30)
        {
            cprCount += count;
            Debug.Log("CPRCount = " + cprCount.ToString());
            erik.say(cprCount.ToString());

            if (cprCount == 1) {
                breathCount = 0;
            }
        }

        updateCounterBar.udpateCounters();

	}

    public void incrementBreathCounter(int count)
    {
        if (breathCount < 2)
        {
            breathCount += count;
            Debug.Log("breathCount = " + breathCount.ToString());
            erik.say(breathCount.ToString());

            if (breathCount == 2)
            {
                cprCount = 0;
            }
        }
        
        
        updateCounterBar.udpateCounters();
    }

    private void Update() {
        
    }
}
