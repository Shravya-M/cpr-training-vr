﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChestCompress : MonoBehaviour
{
    public enum ChestCompressState
    {
        HandAway,  //Blue
        HandInPosition, // Cyan
        HandOn, // Yellow
        HandIn, // Green
        ChestBreak, //Red
    };
    GameObject canvas;
    GameObject breastL;
    GameObject breastR;
    GameObject bothhand;
    GameObject compPoint;
    GameObject breastStartRef;
    private float HandPos;
    private float startPos;
    private float distance;
    private ChestCompressState state;
    public CommonTracker rightController;
    public CommonTracker leftController;
    public Material chestGreenMaterial;
    public Material chestRedMaterial;
    public Material chestTransparentMaterial;
    Material[] mat;
    bool xAndz;
    
    Counter c = new Counter();
    static int compressionFlag = 0;

    bool chestBreak;
    bool compression;

   



    Shader shader1;
    Shader shader2;

    Renderer rend;

    AudioSource audioSrc;

    // Use this for initialization
    void Start()
    {

        //canvas = GameObject.FindGameObjectWithTag("canvas");
        xAndz = false;
        bothhand = GameObject.FindGameObjectWithTag("CurledHand");
        breastL = GameObject.FindGameObjectWithTag("Breast.L");
        breastR = GameObject.FindGameObjectWithTag("Breast.R");
        breastStartRef = GameObject.FindGameObjectWithTag("BreastStartRef");
        rend = breastStartRef.GetComponent<Renderer>();
        shader1 = Shader.Find("Transparent/Diffuse");
        shader2 = Shader.Find("Standard");
        rend.material.shader = shader2;
        rend.material.color = Color.blue;
        startPos = breastStartRef.transform.position.y;
        state = ChestCompressState.HandAway;
        //HandPos = Math.Abs(rightController.GetVirtualPosition().y + leftController.GetVirtualPosition().y) / 2;
        compPoint = GameObject.FindGameObjectWithTag("compressionPoint");
        audioSrc = breastStartRef.GetComponent<AudioSource>();
        compression = false;
        chestBreak = false;
        breastL.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
        breastR.transform.position = new Vector3(0.3f, 0.02f, -1.4f);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Grap the bothand instance with every frame change
        bothhand = GameObject.FindGameObjectWithTag("CurledHand");
        float low = -0.12f;
        float warning = -0.14f;
        float bottom = -0.16f;
        // if hands are crossed
        if (bothhand != null)
        {
            compPoint = GameObject.FindGameObjectWithTag("compressionPoint");
            compPoint.SetActive(true);
            HandPos = compPoint.transform.position.y;
            distance = HandPos - startPos;
            float bothHandXDist = (compPoint.transform.position.x);
            float bothHandZDist = (compPoint.transform.position.z);
            float xdist = Math.Abs(bothHandXDist - breastStartRef.transform.position.x);
            float zdist = Math.Abs(bothHandZDist - breastStartRef.transform.position.z);


            //print("compression:  " + compression.ToString() + "\n chest break: " + chestBreak.ToString());
            // Hands within the boundary
            if ((bothHandXDist > 0.284 && bothHandXDist < 0.376) && (bothHandZDist > -1.358 && bothHandZDist < -1.305))
            {
                //print(" distance: "+ distance + "\tstate" + state.ToString());
                rend.material.color = Color.cyan;
                //compressionFlag = 0;

                // From HandAway
                if (state == ChestCompressState.HandAway)

                {
                    //rend.material.color = Color.blue;
                    if (distance > 0 && bothhand.activeSelf)
                    {
                        // keep the state unchange
                        //print("HandAway to HandAway");
                        state = ChestCompressState.HandAway;
                       // breastL.transform.position = new Vector3(0.3f, -0.00f, -1.4f);
                        //breastR.transform.position = new Vector3(0.3f, -0.00f, -1.4f);
                    }
                    else if (distance < 0 && bothhand.activeSelf)
                    {
                        // change the state to hands on chest
                        //print("HandAway to HandInPos");
                        state = ChestCompressState.HandInPosition;
                    }
                }

                //From HandInPosition
                else if (state == ChestCompressState.HandInPosition)
                {
                    rend.material.color = Color.cyan;
                    if (compressionFlag == 1)
                    {
                        compressionFlag = 0;
                        c.updateCPRCounter(1);
                    }
                    else if (compressionFlag == -1)
                    {
                        compressionFlag = 0;
                        c.updateCPRCounter(-1);
                    }
                    else if (compressionFlag == 3)
                    {
                        compressionFlag = 0;
                        c.updateCPRCounter(3);
                    }
                    //compressionFlag = 0;
                    //HandInPos to HandAway
                    if (!((bothHandXDist > 0.284 && bothHandXDist < 0.376) && (bothHandZDist > -1.358 && bothHandZDist < -1.305)))
                    {
                        //print("HandInPos to HandAway");
                        state = ChestCompressState.HandAway;

                    }
                    //HandInPos to HandInPos
                    else if (distance > 0 && bothhand.activeSelf)
                    {
                        //print("HandInPos to HandInPos");
                        state = ChestCompressState.HandInPosition;
                    }
                    //HandInPos to HandOn
                    else if (distance > -0.10 && distance < 0 && bothhand.activeSelf)
                    {
                        //print("HandInPos to HandOn");
                        state = ChestCompressState.HandOn;
                    }
                }

                //From HandOn 
                else if (state == ChestCompressState.HandOn)
                {
                    
                    rend.material.color = Color.yellow;
                    
                    //HandOn to HandInPos
                    if (distance > 0 && bothhand.activeSelf)
                    {
                        //print("HandOn to HandInPos");
                        state = ChestCompressState.HandInPosition;

                        //checking for chest break and proper compression
                        if (compression)
                        {
                            //print("PROPER COMEPRESSION DONE");
                            compression = false;
                            
                        }
                        else if (chestBreak)
                        {
                           // print("CHEST BROKEN");
                            chestBreak = false;
                            
                        }
                    }
                    //HandOn to HandOn
                    else if (distance > -0.10 && distance < 0 && bothhand.activeSelf)
                    {
                        //print("HandOn to HandOn");
                        state = ChestCompressState.HandOn;
                    }
                    //HandOn to HandIn
                    else if (distance > low && distance < -0.10 && bothhand.activeSelf)
                    {
                       //print("HandOn to HandIn");
                        state = ChestCompressState.HandIn;
                        //chest compression
                        breastL.transform.position = new Vector3(0.3f, -0.06f, -1.4f);
                        breastR.transform.position = new Vector3(0.3f, -0.06f, -1.4f);
                    }
                }

                //From HandIn 
                else if (state == ChestCompressState.HandIn)
                {
                    rend.material.color = Color.green;
                    if (compressionFlag != -1)
                        {
                            compressionFlag = 1;
                        }

                    if (chestBreak)
                    {
                        compression = false;

                    }
                    else
                    {
                        compression = true;
                    }
                    
                    //HandIn to HandOn
                    if (distance > -0.10 && distance < 0 && bothhand.activeSelf)
                    {
                        //print("HandIn to HandOn");
                        rend.material.color = Color.yellow;
                        state = ChestCompressState.HandOn;
                        if(chestBreak || compression)
                        {
                            //normal chest
                            breastL.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
                            breastR.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
                        }
                    }
                    //HandIn to ChestBreak
                    else if (distance < low && bothhand.activeSelf)

                    {
                        //print("HandIn to ChestBreak");
                        state = ChestCompressState.ChestBreak;
                        //chest break
                        audioSrc.Play();
                    }
                    //HandIn to HandIn
                    else if (distance > low && distance < -0.10 && bothhand.activeSelf)
                    {
                        //compression = true;
                        //print("HandIn to HandIn");
                        state = ChestCompressState.HandIn;
                    }
                }

                //From ChestBreak 
                else if (state == ChestCompressState.ChestBreak)
               
                {
                     //print("Chest Break");
                    rend.material.color = Color.red;
                    compressionFlag = -1;
                    breastL.transform.position = new Vector3(0.3f, -0.08f, -1.4f);
                    breastR.transform.position = new Vector3(0.3f, -0.08f, -1.4f);
                    // if (compression)
                    // {
                    //     compression = false;
                    //     chestBreak = true;
                    // }
                    // else
                    // {
                    //     chestBreak = true;
                    // }

                    //ChestBreak to ChestBreak
                    if (distance < low && bothhand.activeSelf)
                    {
                         //print("ChestBreak to ChestBreak");
                        if(distance < warning && distance > bottom )
                        {
                            chestBreak = false;
                            compression = true;
                        }
                        else
                        {
                            compression = false;
                            chestBreak = true;
                         }
                        state = ChestCompressState.ChestBreak;
                    }
                    //ChestBreak to HandIn
                    else if (distance > low && distance < -0.10 && bothhand.activeSelf)
                    {
                       //print("ChestBreak to HandIn");
                        //compression = false;
                        state = ChestCompressState.HandIn;
                    }
                    else{
                        //print("ChestBreak to Handawary");
                        state =ChestCompressState.HandAway;
                    }
                }
            }
            else
            {
                //print("hands not in range");
                rend.material.color = Color.blue;
                //compressionFlag = 0;
                state = ChestCompressState.HandAway;
                //if hands come out of range in between
                compression = false;
                chestBreak = false;
                breastL.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
                breastR.transform.position = new Vector3(0.3f, 0.02f, -1.4f);

            }

        }
        // hands uncrossed
        else
        {
            //print("The hands are not crossed");
            // The hands are not crossed
            state = ChestCompressState.HandAway;
            if (compressionFlag == 1)
            {
                compressionFlag = 3;
            }
            compPoint = null;
            rend.material.shader = shader2;
            rend.material.color = Color.blue;
            //compressionFlag = 0;
            breastL.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
            breastR.transform.position = new Vector3(0.3f, 0.02f, -1.4f);
        }
    }
}

