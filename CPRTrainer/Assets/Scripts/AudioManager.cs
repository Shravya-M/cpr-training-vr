﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour {

    // Use this for initialization

    public CustomSound[] sounds;
	void Awake () {
		foreach(CustomSound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
        }
	}

    public void Play(string name)
    {
        print(sounds.Length);
        CustomSound s = Array.Find(sounds, sound => sound.name == name);
        if (!s.source.isPlaying)
        {
            s.source.Play();
        }
    }
}
