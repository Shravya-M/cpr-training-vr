﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EricJaw : MonoBehaviour {

    public GameObject ericJaw;

	// Use this for initialization
	void Start () {
        ericJaw = GameObject.FindGameObjectWithTag("EricJaw");
        stopJaw();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void startJaw() {
        ericJaw = GameObject.FindGameObjectWithTag("EricJaw");
        ericJaw.GetComponent<Animator>().enabled = true;
    }

    public void stopJaw() {
        ericJaw = GameObject.FindGameObjectWithTag("EricJaw");
        ericJaw.GetComponent<Animator>().enabled = false;
    }
}
