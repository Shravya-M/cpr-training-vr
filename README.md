# CPR Training VR
CPR Trainer

Overview: 

This virtual reality project is designed to teach the user how to properly perform CPR. It will teach the user how to rotate the patients head back, how to open the mouth, how to perform mouth-to-mouth, how to properly hold one’s hands, where to properly place one’s hands, and how to perform chest compressions.


Technical Details:

The project is located in the scene: SampleScene 

This project was created in and for Unity version 2018.1.4f1. This project was also created to be used with the HTC Vive. 

Upon downloading the project, simply open up this version of Unity and select the “CPR Trainer” Folder to load the project. 


Virtual Controls:

There are three pieces of physical hardware that should be utilized while running this application: Vive HMD, Vive Right Hand Controller, and Vive Left Hand Controller. These devices should be used as follows:

Vive HMD: Once the HMD is properly on one’s head and within the sensors, this device will not only be used for virtual reality, but also for user tracking within the virtual world

Vive Right Hand Controller: Used for chest compressions, head tilting, and breathing

Vive Left Hand Controller: Used for chest compressions, opening the jaw, head tilting, and PowerPoint manipulation

User Actions:

-	Head tilting: Place the left hand on the jaw and the right hand on the forehead. Once both hands are interacting with the respective objects, place both trigger buttons down and rotate the head back.

-	Opening the jaw: Place the left hand on the jaw. Once the left hand is interacting with the jaw, press the trigger button down and pull the jaw down.

-	Chest compressions: Chest compressions can only be completed with both hands. To create a “both hands” image, cross the remotes over each other or place them right next to each. Once this is done, place the two hands within the marker on the dummy’s chest. While the object is yellow, press down. Once the object is green, you have gone the desired distance. If the object turns red, you have gone too far and potentially injured your patient.

-	Mouth-to-mouth and breathing: In order to perform mouth to mouth, the user must be very close to the dummy’s mouth. Once they are within range, press the right touchpad button for the breathing audio. After a predetermined number of breaths, the dummy will produce a sound equal to that of them “awaking.”

-	PowerPoint slide movement: To change the PowerPoint slides, use the left touchpad button. Press the right side to go forward, and the left side to go backwards.



